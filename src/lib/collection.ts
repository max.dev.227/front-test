import { BASE_URL } from '../helper/config';

export interface CARD {
  id: number,
  player: {
    firstname: string,
    lastname: string,
    birthday: string
  }
}

export function fetchCollection(): Promise<void | Array<CARD>> {
  /**
   * Step 2: Instead of directly returning the collection, fetch it from http://localhost:8001/cards
   */

  return fetch(`${BASE_URL}/cards`)
    .then(res => {
      if(!res.ok) throw new Error(res.statusText); 
      return res.json()
    })

  // return [
  //   {
  //     id: 1,
  //     player: {
  //       firstname: 'John',
  //       lastname: 'Doe',
  //       birthday: '1993-07-22T08:38:50.090Z',
  //     },
  //   },
  // ];
}

import React, { useState, useEffect } from 'react';

import { fetchCollection, CARD } from '../lib/collection';
import './Collection.css';

interface CardItemProps {
  card: CARD;
}

// Card Component
const CardItem: React.FC<CardItemProps> = ({ card }) => {
  return (
    <div className="card">
      <div className="row">
        <label>First Name:</label>
        <div>{card.player.firstname}</div>
      </div>
      <div className="row">
        <label>Last Name:</label>
        <div>{card.player.lastname}</div>
      </div>
      <div className="row">
        <label>Birthday:</label>
        <div>{card.player.birthday}</div>
      </div>
    </div>
  );
};

export const Collection = () => {
  const [loading, setLoading] = useState(true);
  const [collection, setCollection] = useState<CARD[]>([]);

  // Getting the collection from backend
  useEffect(() => {
    const fetchCards = async () => {
      try {
        setLoading(true);
        const res = await fetchCollection();
        setCollection(res || []);
      } catch (error) {
        console.error(error);
      } finally {
        setLoading(false);
      }
    };

    fetchCards();
  }, []);

  if (loading) {
    return <div className="loading">Loading...</div>;
  }

  // if collection is empty array
  if (collection.length === 0) {
    return <div className="empty">No player</div>;
  }

  /**
   * Step 1: Render the card
   */
  return (
    <div className="collection-container">
      {collection.map((card) => (
        <CardItem key={card.id} card={card} />
      ))}
    </div>
  );
};

import React, { useCallback } from 'react';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';

import { BASE_URL } from '../helper/config';
import './CreateCard.css';

/**
 * Step 3: Render a form and everything needed to be able to create a card
 */

const CreateCardSchema = Yup.object().shape({
  firstname: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  lastname: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  birthday: Yup.date().required('Required'),
});

export const CreateCard = () => {
  // Handler to save a new card
  const handleCreate = useCallback(({ firstname, lastname, birthday }) => {
    const param = {
      player: {
        firstname,
        lastname,
        birthday,
      },
    };

    fetch(`${BASE_URL}/cards`, {
      method: 'POST',
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      },
      body: JSON.stringify(param),
    })
      .then(() => alert('Success'))
      .catch((e) => {
        console.log('error', e);
        alert('Error');
      });
  }, []);

  return (
    <div className="container">
      <Formik
        initialValues={{
          firstname: '',
          lastname: '',
          birthday: '',
        }}
        validationSchema={CreateCardSchema}
        onSubmit={handleCreate}
      >
        {({ errors, touched }) => (
          <Form>
            <div className="row">
              <label htmlFor="firstname">First Name: </label>
              <div>
                <Field name="firstname" />
                {errors.firstname && touched.firstname ? (
                  <div className="validation-error">{errors.firstname}</div>
                ) : null}
              </div>
            </div>
            <div className="row">
              <label htmlFor="firstname">Last Name: </label>
              <div>
                <Field name="lastname" />
                {errors.lastname && touched.lastname ? (
                  <div className="validation-error">{errors.lastname}</div>
                ) : null}
              </div>
            </div>
            <div className="row">
              <label htmlFor="birthday">Birthday: </label>
              <div>
                <Field name="birthday" type="date" />
                {errors.birthday && touched.birthday ? (
                  <div className="validation-error">{errors.birthday}</div>
                ) : null}
              </div>
            </div>
            <div className="row button-container">
              <button type="submit">Create</button>
            </div>
          </Form>
        )}
      </Formik>
    </div>
  );
};
